package bbs.service;

import static bbs.utils.CloseableUtil.*;
import static bbs.utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import bbs.beans.Position;
import bbs.dao.PositionDao;

public class PositionService {


	public List<Position> getPositions() {

		Connection connection = null;
		try {
			connection = getConnection();

			PositionDao PositionDao = new PositionDao();
			List<Position> ret = PositionDao.getPositions(connection);

			commit(connection);

			return ret;
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}
}
