package bbs.service;

import static bbs.utils.CloseableUtil.*;
import static bbs.utils.DBUtil.*;

import java.sql.Connection;

import bbs.beans.Contribution;
import bbs.dao.ContributionDao;

public class ContributionService {

	public void register(Contribution contribution) {

		Connection connection = null;
		try {
			connection = getConnection();

			ContributionDao contributionDao = new ContributionDao();
			contributionDao.insert(connection, contribution);

			commit(connection);

			commit(connection);
		} catch (RuntimeException e) {
			rollback(connection);
			throw e;
		} catch (Error e) {
			rollback(connection);
			throw e;
		} finally {
			close(connection);
		}
	}

}
