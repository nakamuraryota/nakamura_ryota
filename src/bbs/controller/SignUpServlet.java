package bbs.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import bbs.beans.Branch;
import bbs.beans.Position;
import bbs.beans.User;
import bbs.service.BranchService;
import bbs.service.PositionService;
import bbs.service.UserService;

@WebServlet(urlPatterns = { "/signup" })
public class SignUpServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {

		request.getRequestDispatcher("signup.jsp").forward(request, response);
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {

		List<String> messages = new ArrayList<String>();
		HttpSession session = request.getSession();

			User user = new User();
			user.setUser_id(request.getParameter("user_id"));
			user.setPassword(request.getParameter("password"));
			user.setName(request.getParameter("name"));
			user.setBranch_id(request.getParameter("branch_id"));
			user.setPosition_id(request.getParameter("position_id"));

		if (isValid(request, messages) == true) {
			new UserService().register(user);
			response.sendRedirect("./");
		} else {
			List<Branch> branches = new BranchService().getBranches();
			request.setAttribute("branches", branches);
			List<Position> positions = new PositionService().getPositions();
			request.setAttribute("positions", positions);

			session.setAttribute("errorMessages", messages);
			request.setAttribute("editUser", user);
			request.getRequestDispatcher("signup.jsp").forward(request, response);
//			response.sendRedirect("signup");
		}
	}

	private boolean isValid(HttpServletRequest request, List<String> messages) {
		String user_id = request.getParameter("user_id");
		String password = request.getParameter("password");
		String name = request.getParameter("name");
		String branch_id = request.getParameter("branch_id");
		String position_id = request.getParameter("position_id");
		String confirm_password = request.getParameter("confirm_password");

		if (StringUtils.isEmpty(user_id) == true) {
			messages.add("ログインIDを入力してください");
		}else{
			if(user_id.length() < 6 || user_id.length() > 20){ //ログインIDの文字列が6より小さい場合と、20より大きい場合のエラー処理
				messages.add("ログインIDは6文字以上20文字以下で入力してください");
			}
			if(!user_id.matches("[\\w]+")){ //半角英数字以外が含まれている場合のエラー処理
				messages.add("ログインIDは半角英数字で入力してください");
			}
		}
		if(StringUtils.isEmpty(password) == true) {
			messages.add("パスワードを入力してください");
		}else{
			if(password.length() < 6 || password.length() > 20 ){ //パスワードの文字列が6より小さく20より大きい場合のエラー処理
				messages.add("パスワードは6文字以上20文字以下で入力してください");
			}
			if(!password.matches("[-_@+*;:#$%&\\w]+")){ //半角英数記号以外が含まれている場合のエラー処理
				messages.add("パスワードは記号を含む全ての半角文字で入力してください");
			}
		}
		if(!password.equals(confirm_password)){ //文字列が一致するという場合はequalsを使う。
			messages.add("パスワードが一致していません。");
		}
		if (StringUtils.isEmpty(name) == true) {
			messages.add("ユーザー名を入力してください");
		}
		if(name.length() > 10){ //ユーザー名が10文字以上が出力された場合のエラー処理
			messages.add("ユーザー名は10文字以下で入力してください");
		}
		if(StringUtils.isEmpty(branch_id) == true) {
			messages.add("配属店を入力してください");
		}
		if(StringUtils.isEmpty(position_id) == true) {
			messages.add("部署・役職番号を入力してください");
		}

		// TODO アカウントが既に利用されていないか、メールアドレスが既に登録されていないかなどの確認も必要
		if (messages.size() == 0) {
			return true;
		} else {
			return false;
		}
	}

}