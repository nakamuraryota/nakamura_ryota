package bbs.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import bbs.beans.Branch;
import bbs.beans.Position;
import bbs.beans.User;
import bbs.service.BranchService;
import bbs.service.PositionService;
import bbs.service.UserService;

@WebServlet(urlPatterns = { "/settings" })
public class SettingsServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();

   		String id = request.getParameter("id");
        //セッションよりログインユーザーの情報を取得
//        User loginUser = (User) session.getAttribute("loginUser");
        //ログインユーザー情報のidを元にDBからユーザー情報取得
        User editUser = new UserService().getUser(id);
        request.setAttribute("editUser", editUser);

        List<Branch> branches = new BranchService().getBranches();
//        request.getRequestDispatcher("settings.jsp").forward(request, response);
        request.setAttribute("branches" , branches);

        List<Position> positions = new PositionService().getPositions();
        request.setAttribute("positions", positions);

		if(editUser==null){
			List<String> messages = new ArrayList<String>();
			messages.add("不正なパラメーターです");

			response.sendRedirect("management");
			session.setAttribute("errorMessages", messages);
		}else {
			request.getRequestDispatcher("setting.jsp").forward(request, response);
		}
	}
}