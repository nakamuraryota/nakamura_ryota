package bbs.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import bbs.beans.Contribution;
import bbs.beans.User;
import bbs.service.ContributionService;

@WebServlet(urlPatterns = { "/newContribution" })
public class NewContribution extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse responce)
			throws IOException, ServletException {
		request.getRequestDispatcher("newContribution.jsp").forward(request,responce);
	}
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws IOException, ServletException {

		HttpSession session = request.getSession();

		List<String> messages = new ArrayList<String>();

			User user = (User) session.getAttribute("loginUser");

			Contribution contribution = new Contribution();
			contribution.setTitle(request.getParameter("title"));
			contribution.setText(request.getParameter("text"));
			contribution.setCategory(request.getParameter("category"));
			contribution.setUser_id(user.getId());

		if (isValid(request, messages) == true) {
			new ContributionService().register(contribution);
			response.sendRedirect("./");
		} else {
			session.setAttribute("errorMessages", messages);
			session.setAttribute("contribution", contribution);
			response.sendRedirect("newContribution.jsp/");
		}
	}

	private boolean isValid(HttpServletRequest request, List<String> messages) {

		String title = request.getParameter("title");
		String text = request.getParameter("text");
		String category = request.getParameter("category");

		if (StringUtils.isEmpty(title) == true){
			messages.add("タイトルを入力してください");
		} else if (30 < title.length()) {
			messages.add("タイトルは30文字以下で入力してください");
		}
		if (StringUtils.isEmpty(text) == true) {
			messages.add("本文を入力してください");
		} else if (1000 < text.length()) {
			messages.add("本文は1000文字以下で入力してください");
		}
		if (StringUtils.isEmpty(category) == true){
			messages.add("カテゴリーを入力してください");
		} else if (10 < category.length()){
			messages.add("カテゴリー名は10文字以下で入力してください");
		}
		if (messages.size() == 0) {
			return true;
		} else {
			return false;
		}
	}

}