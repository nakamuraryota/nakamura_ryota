package bbs.beans;


import java.io.Serializable;
import java.util.Date;

public class UserName implements Serializable {
	private static final long serialVersionUID = 1L;

	private int id;
	private String user_id;
	private String password;
	private String name;
	private String branch_id;
	private String position_id;
	private Date created_date;

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getUser_id() {
		return user_id;
	}
	public void setUser_id(String user_id) {
		this.user_id = user_id;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getBranch_id() {
		return branch_id;
	}
	public void setBranch_name(String branch_id) {
		this.branch_id = branch_id;
	}
	public String getPosition_name() {
		return position_id;
	}
	public void setPosition_name(String position_id) {
		this.position_id = position_id;
	}
	public Date getCreated_date() {
		return created_date;
	}
	public void setCreated_date(Date created_date) {
		this.created_date = created_date;
	}
}