package bbs.beans;

import java.io.Serializable;
import java.util.Date;

public class Contribution implements Serializable {
	private static final long serialVersionUID = 1L;

	private int id;
	private String title;
	private String text;
	private String category;
	private int user_id;
	private Date createdDate;
	private Date updatedDate;

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id =id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title){
		this.title =title;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text =text;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category){
		this.category = category;
	}
	public int getUser_id() {
		return user_id;
	}
	public void setUser_id(int user_id){
		this.user_id = user_id;
	}
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	public Date getUpdatedDate(){
		return updatedDate;
	}
	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}
}
