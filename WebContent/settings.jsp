<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>ユーザー編集</title>
<link href="css/style.css" rel="stylesheet" type="text/css">
</head>
<body>
	<div class="main-contents">
		<div class="header">
			ユーザー情報編集
		</div>
		<div class = link>
			<a href="./">ホーム</a>
			<a href="setting">戻る</a>
		</div>

		<c:if test="${ not empty errorMessages }">
			<div class="errorMessages">
				<ul>
					<c:forEach items="${errorMessages}" var="message">
						<li><c:out value="${message}" />
					</c:forEach>
				</ul>
			</div>
			<c:remove var="errorMessages" scope="session" />
		</c:if>
		<form action="setting" method="post"><br />

			<input name="id" value="${editUser.id}" id="id" type="hidden"/>

			<label for="user_id">ログインID</label><br />
			<input name="user_id" value="${editUser.login_id}" id="login_id"/><br />

			<label for="password">パスワード</label><br />
			<input name="password" type="password" id="password" /><br />

			<label for="passwordConfirm">パスワード（確認）</label><br />
			<input type="password" name="passwordConfirm" id="passwordConfirm"><br />

			<label for="name">名前</label><br />
			<input name="name" value="${editUser.name}" id="name" /><br />

			<c:if test="${loginUser.id != editUser.id}">
				<label for="branch_id">配属店</label><br />
				<select name="branch_id">
					<c:forEach items="${branches}" var="branch">
						<c:if test="${editUser.branch_id != branch.id}">
							<option value="${branch.id}">${branch.name}</option>
						</c:if>
						<c:if test="${editUser.branch_id == branch.id}">
							<option value="${branch.id}"selected>${branch.name}</option>
						</c:if>
					</c:forEach>
				</select>
				<br />
				<label for="position_id">部署・役職</label><br />
				<select name="position_id">
					<c:forEach items="${positions}" var="position">
						<c:if test="${editUser.position_id != position.id}">
							<option value="${position.id}">${position.name}</option>
						</c:if>
						<c:if test="${editUser.position_id == position.id}">
							<option value="${position.id}"selected>${position.name}</option>
						</c:if>
					</c:forEach>
				</select>
				<br />
			</c:if>
			<c:if test="${loginUser.id == editUser.id}">
				<input type="hidden" name="branch_id" value="${loginUser.branch_id}" />
				<input type="hidden" name="position_id" value="${loginUser.position_id}" />
			</c:if>
			<input type="submit" value="登録" /> <br />
		</form>
		<br>
		</div>
	</body>
</html>
