<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>掲示板</title>
</head>
<body>
	<div class="main-contents">
		<div class="header">
			掲示板
		</div>

			<c:if test="${ empty loginUser }">
				<center>
					<a href="login">ログイン</a>
					<a href="signup">登録する</a>
				</center>
			</c:if>
			<c:if test="${ not empty loginUser }">
				<a href="./">ホーム</a>
				<a href="settings">設定</a>
				<a href="logout">ログアウト</a>
			</c:if>
		</div>
		<c:if test="${ not empty loginUser }">
			<div class="profile">
				<div class="name"><h2><c:out value="${loginUser.name}" /></h2></div>
				<div class="user_id">
					@<c:out value="${loginUser.user_id}" />
				</div>
				<div class="form-area">
					<c:if test="${ isShowMessageForm }">
					<form action="NewContribution" method="post">
					コメント<br />
					<textarea name="contribution" cols="100" rows="5" class="contribution-box"></textarea>
					<br />
					<input type="submit" value="投稿する"> (1000文字まで)
				</form>
			</c:if>
		</div>

			</div>
		</c:if>
		<br>
</body>
</html>
