<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link rel="stylesheet" href="./style.css">
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>新規投稿</title>
</head>
<body>
<div class="main-contents">
<div class="title"><h3>新規投稿</h3></div>

			<c:if test="${ not empty errorMessages }">
				<div class="errorMessages">
					<ul>
						<c:forEach items="${errorMessages}" var="message">
							<li><c:out value="${message}" />
						</c:forEach>
					</ul>
				</div>
				<c:remove var="errorMessages" scope="session"/>
			</c:if>

                <form action="post" method="post"> <br />

            	<label for="title">件名</label>
            	<input name="title" id="title" value="${subject}"/> （30文字まで）<br />

				<label for="text">本文</label>
				<textarea name="text" cols="50" rows="10" class="tweet-box" >${text}</textarea>（1000文字まで）<br />


                <label for="category">カテゴリ</label>
            	<input name="category" id="category" value="${category}" /> （10文字まで）<br />

                <input type="submit" value="投稿" /> <br />
                <a href="./">戻る</a>
            </form>
        </div>
</body>
</html>