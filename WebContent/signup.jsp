<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>ユーザー新規登録</title>
</head>
<body>
	<div class="main-contents">
		<c:if test="${ not empty errorMessages }">
			<div class="errorMessages">
				<ul>
					<c:forEach items="${errorMessages}" var="message">
						<li><c:out value="${message}" />
					</c:forEach>
				</ul>
			</div>
			<c:remove var="errorMessages" scope="session" />
		</c:if>
		<form action="signup" method="post">

			<br /> <label for="user_id">ログインID </label> <input name="user_id" id="user_id" />
			<br /> <label for="name">ユーザー名</label> <input name="name" id="name" />
			<br /> <label for="password">パスワード</label> <input name="password" type="password" id="password" />
			<br /> <label for="confirm_password">パスワード（確認）</label> <input name ="confirm_password" type="password" id="confirm_password" />
			<br /> <label for="branch_id">配属店</label> <input name="branch_id" id="branch_id" />
			<br /> <label for="position_id">部署・役職</label> <input name="position_id" id="position_id" />

			<br /> <input type="submit" value="登録" /> <br /> <a href="./">戻る</a>
		</form>
	</div>
</body>
</html>